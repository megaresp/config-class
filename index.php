<?php

declare(strict_types=1);

namespace WDF;

require 'Config' . DIRECTORY_SEPARATOR . 'Config.php';
$cfg = new Config\Config();

/**
 * Placeholder function that outputs the content of $data
 * in a browser-friendly format.
 */
function pr ($data)
{
    echo '<pre>';

    if (is_object($data) || is_array($data))
        echo print_r($data, true);
    else
        echo $data;
    
    echo '</pre>';
}

/**
 * Output the 'system' array from Config::Data that is
 * contained in Data.php
 */
pr($cfg->get('system'));

/**
 * Output the specific value in Config::Data['site']['name']
 */
pr($cfg->get('system.version'));

/**
 * Outputs 'beyond,' the value located in Data.php and stored
 * in Config::Data['foo']['bar']['baz']
 */
pr($cfg->get('foo.bar.baz'));