# Config Class

Provides a global configuration class to any PHP project. Config allows anything with an object instantiated from the Config class to access the data it contains using dot notation.

You can see a working example of the Config class instantiated and used in index.php.

## Config data

The file Data.php contains a function that returns a PHP array. This data is loaded by the Config class on instantiation and can be retrieved using the method Config::get().

This format allows a relatively human-friendly approach to configuration items in native PHP, and thus no unserialization or decoding is required to make use of it.

The Config path then allows dot notation to be used to find any given data item, and to any depth of nesting. For example, in the demo the path to the string 'beyond' is found at...
```PHP
$data['foo']['bar']['baz'] = 'beyond';
```
Here's how to access that specific data item via the Config class...
```PHP
$cfg = new Config();
$cfg->get('foo.bar.baz');
```
While many levels of nesting is possible I limit myself to 2-3 tiers of nesting.
### Tip

When retrieving several items from the same array parent, load the data into a local array and reference each item. This reduces the number of individual method calls. For example…
```PHP
// Imagine this data
$data = [
    'site' => [
        'name' => 'some_name',
        'version' => 1.5,
        'path' => 'some_path',
        'suppress' => FALSE,
    ]
];
// Load the parent array into a local variable
$site = (object)$cfg->get('site');
// And then access the individual items
echo $site->name;
echo $site->version;
echo $site->path;
echo $site->suppress;
```

## Setup

In the demo I've placed Data.php and Config.php in the same folder. This needn't be the case and a developer is welcome to place these files to suit their own project.

However, it's best to ensure both files are in the same namespace (as in the example).
