<?php

/**
 * Config class data file
 * 
 * @package     The Web Development Framework
 * @version     0.1.4
 * @author      Wayne Davies
 * @copyright   Copyright (C) 2020, Wayne Davies
 * @lincese     Apache 2.0 http://www.apache.org/licenses/
 */

declare(strict_types=1);

namespace WDF\Config;

function get_data (): array
{
    return [
        'system' => [
            'version' => '0.3.6',
            'name' => 'PHP Global Config',
            'author' => 'Wayne Davies',
            'copyright' => 'Copyright &copy; ' . date('Y'),
            'license' => 'Apache License 2.0',
            'licenseurl' => 'http://www.apache.org/licenses/LICENSE-2.0.html',
        ],
        'foo' => [
            'bar' => [
                'baz' => 'beyond',
            ]
        ],
        'db' => [
            'host' => 'localhost',
            'name' => 'mydbname',
            'user' => 'somedbuser',
            'pass' => '884b243ba9abe82f1037e9c8f1462a0b',
            'valid' => ['DELETE', 'INSERT', 'SELECT', 'UPDATE',],
        ],
        'log' => [
            'failed_logins' => TRUE,
            'system' => FALSE,
            'path' => 'Logs',
            'datetime' => 'Y-m-d H:i:s',
        ],
    ];
}