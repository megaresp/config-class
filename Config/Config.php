<?php

/**
 * A PHP Config Class
 * 
 * @package     Config.php
 * @version     0.3.6
 * @author      Wayne Davies
 * @copyright   Copyright (C) 2020, Wayne Davies
 * @license     Apache 2.0 http://www.apache.org/licenses/
 */

declare(strict_types=1);

namespace WDF\Config;

class Config
{
    CONST SEP = '.';
    
    private string $path;
    private array $data;
    
    function __construct ()
    {
        $this->filename = basename(__CLASS__);
        $this->path = 'Data.php';
        $this->load();
    }

    public function get (string $key)
    {
        if (isset($this->data[$key])) return $this->data[$key];

        if (strpos($key, SELF::SEP) !== FALSE)
            $path = explode(SELF::SEP, $key);
        
        if (empty($path) || count($path) === 0) die('::');//return NULL;

        $data = $this->data;
        foreach ($path as $key)
        {
            if (!isset($data[$key])) return FALSE;
            $data = $data[$key];
        }

        return $data;
    }

    private function load ()
    {
        require $this->path;
        $this->data = get_data();
    }
}